class Appointment < ActiveRecord::Base
  attr_accessible :due_at, :email, :sms, :title
  belongs_to :user
end
