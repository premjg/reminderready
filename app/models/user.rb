class User < ActiveRecord::Base
  attr_accessible :email, :name, :payroll_number, :phone_number, :password

  has_many :appointments
end
