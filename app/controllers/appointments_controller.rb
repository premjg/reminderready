class AppointmentsController < ApplicationController
  before_filter :get_user

  def index
    @appointments = @user.appointments.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @appointments }
    end
  end

  def show
    @appointment = @user.appointments.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @appointment }
    end
  end

  def new
    @appointment = @user.appointments.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @appointment }
    end
  end

  def edit
    @appointment = @user.appointments.find(params[:id])
  end

  def create
    @appointment = @user.appointments.new(params[:appointment])

    respond_to do |format|
      if @appointment.save
        format.html { redirect_to @appointment, notice: 'User was successfully created.' }
        format.json { render json: @appointment, status: :created, location: user_appointment_path(@user, @appointment) }
      else
        format.html { render action: "new" }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @appointment = @user.appointments.find(params[:id])

    respond_to do |format|
      if @appointment.update_attributes(params[:appointment])
        format.html { redirect_to user_appointment_path(@user, @appointment), notice: 'User was successfully updated.' }
        format.json { render json: @appointment, location: user_appointment_path(@user, @appointment)}
      else
        format.html { render action: "edit" }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @appointment = @user.appointments.find(params[:id])
    @appointment.destroy

    respond_to do |format|
      format.html { redirect_to user_appointments_path(@user) }
      format.json { head :no_content }
    end
  end

  private
  def get_user
    @user = User.find(params[:user_id])
  end
end
