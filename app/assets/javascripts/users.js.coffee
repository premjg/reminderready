window.populateAppointment = (data) ->
  _.templateSettings = interpolate : /\{\{(.+?)\}\}/g
  template = _.template($("#appointment-template").html(), data, {variable: 'data'})
  $("#appointments-container").prepend(template)

  # Delete Button, unload and load events
  $(".deleteAppointment").off 'click'
  $(".editAppointment").off 'click'
  $(".deleteAppointment").on 'click', (e) ->
    e.preventDefault()
    bootbox.confirm "Are you sure you want delete this appointment?", (result) =>
      if result
        elem = $(@).parents("li")
        data = $(elem).data("appointment")
        userId = $('#user-details').data('user')['id']
        $.ajax
          type: 'delete'
          url: "/users/#{userId}/appointments/#{data.id}"
          dataType: 'json'
          success: (data) ->
            $(elem).remove()
          error: (err) ->
            console.log err

  # Edit Appointment, unload and load events
  $(".editAppointment").on 'click', (e) ->
    elem = $(@).parents("li")
    rawData = $(elem).data("appointment")
    userId = $('#user-details').data('user')['id']

    data =
      form_heading: "Edit Appointment"
      save_label: 'Save'
      title: rawData.title
      date: moment(rawData.due_at).format('MM-DD-YYYY')
      time: moment(rawData.due_at).format('hh:mm a')      
      email: rawData.email
      sms: rawData.sms

    template = _.template($("#appointment-form-template").html(), data)
    $("#modal-form-container").html(template)
    $("#modal-form-container #add-appointment-modal").modal("show")
    $("#modal-form-container #add-appointment-modal").on "shown", () ->
      $('#appointmentTime').timepicker({defaultTime: 'value'});
      $('#datepicker').datepicker({
        date: data.date
        format: 'mm-dd-yyyy'
        autoclose: true
      })
      # Checkbox UI fix
      $("#appointmentEmail, #appointmentSms").each () ->
        if $(@).val() == "true" then $(@).attr("CHECKED", true)

    # Save Button
    $("#save-appointment").off 'click'
    $("#save-appointment").on 'click', (e) ->
      $('#add-appointment-form:eq(0)').submit()
    
    # Form Submission
    $("#add-appointment-form").off 'submit'
    $('#add-appointment-form').on 'submit', (e) ->
      e.preventDefault()

      userId = $('#user-details').data('user')['id']
      url = "/users/#{userId}/appointments/#{rawData.id}"
      due_at = moment($('#add-appointment-form input#appointmentDate').val() + " " + $('#add-appointment-form input#appointmentTime').val()).format("dddd, MMMM Do YYYY, h:mm:ss a");
      title = $('#add-appointment-form textarea#appointmentTitle').val()
      email = $('#add-appointment-form input#appointmentEmail').is(':checked')
      sms = $('#add-appointment-form input#appointmentSms').is(':checked')

      data =
        due_at: due_at
        title: title
        email: email
        sms: sms

      $.ajax
        type: 'put'
        url: url
        data: {appointment: data}
        dataType: 'json'
        success: (data) ->
          $(elem).remove()
          window.populateAppointment(data)
          $("#add-appointment-modal").modal("hide")
        error: (err) ->
          console.log err

$ ->
  # Mustache Style Templating
  _.templateSettings = interpolate : /\{\{(.+?)\}\}/g

  # Add all appointments
  _.each $("#appointments-container").data("appointments"), (item) -> window.populateAppointment(item)

  # Show User details
  $("#user-status").popover({
    content: ->
      $("#user-details").html()
    placement: "bottom"
  })

  # Add appointment Click
  $('.add-appointment').click (e) ->
    e.preventDefault()
    data =
      form_heading: "Add a new Appointment"
      save_label: 'Add'
      title: ''
      date: moment(Date.new).format('MM-DD-YYYY')
      time: moment(Date.new).format('hh:mm a')      
      email: false
      sms: false

    template = _.template($("#appointment-form-template").html(), data)
    $("#modal-form-container").html(template)
    $("#modal-form-container #add-appointment-modal").modal("show")
    $("#modal-form-container #add-appointment-modal").on "shown", () ->
      $('#appointmentTime').timepicker();
      $('#datepicker').datepicker({
        date: data.date
        format: 'mm-dd-yyyy'
        autoclose: true
      })

    # Save Button
    $("#save-appointment").on 'click', (e) ->
      $('#add-appointment-form:eq(0)').submit()
    
    # Form Submission
    $('#add-appointment-form').on 'submit', (e) ->
      e.preventDefault()

      userId = $('#user-details').data('user')['id']
      url = "/users/#{userId}/appointments"
      due_at = moment($('#add-appointment-form input#appointmentDate').val() + " " + $('#add-appointment-form input#appointmentTime').val()).format("dddd, MMMM Do YYYY, h:mm:ss a");
      title = $('#add-appointment-form textarea#appointmentTitle').val()
      email = $('#add-appointment-form input#appointmentEmail').is(':checked')
      sms = $('#add-appointment-form input#appointmentSms').is(':checked')
      data =
        due_at: due_at
        title: title
        email: email
        sms: sms

      $.ajax
        type: 'post'
        url: url
        data: {appointment: data}
        dataType: 'json'
        success: (data) ->
          window.populateAppointment(data)
          $("#add-appointment-modal").modal("hide")
        error: (err) ->
          console.log err