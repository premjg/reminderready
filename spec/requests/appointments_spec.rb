require 'spec_helper'

describe "TasksPage" do
  let(:user) {FactoryGirl.create(:user)}

  describe "while on page" do
    before(:each) do
      appointment = FactoryGirl.create(:appointment, :user_id => user.id, :due_at => Time.parse('15/10/2012 11:30 AM'))
      # Logging in so not impelented 
      visit user_path(user)
    end

    it "shows user name after logging in" do
      page.should have_content('John Smith')
      page.should have_content('Logout')
    end

    it "shows existing appointments", :js => true do 
      within("#appointments-container") do
        find('li').should have_content('Awesome appointment')
      end
    end

    it "shows user details when clicked on user badge", :js => true do
      # pending
      click_link user.name
      page.should have_selector('#user-email', :visible => true)
      find("#user-email").text.should eq(user.email)
    end  

    it "opens appointment form when clicked on add", :js => true do
      click_link "Add An Appointment"
      page.should have_selector("#add-appointment-form", :visible => true)
    end

    it "saves an appointment when form is submitted", :js => true do
      # pending
      click_link "Add An Appointment"
      within("#add-appointment-modal") do
        fill_in 'appointmentTitle', :with => 'My Awesome Appointment'
        fill_in 'appointmentDate', :with => '12-04-2012'
        fill_in 'appointmentTime', :with => '11:30 AM'
        check('appointmentEmail')
        check('appointmentSms')
        click_link('Add')
      end
      within("#appointments-container") do
        find('li').should have_content('My Awesome Appointment')
      end
    end

    it "deletes an appointment when delete button is clicked with confirmation", :js => true do
      # pending
      click_link "Delete Appointment"
      page.should have_selector(".bootbox.modal", :visible => true)
      within(".bootbox.modal") do
        click_link "OK"
      end
      within("#appointments-container") do
        page.should_not have_selector("li")
      end
    end

    it "does not delete an appointment when delete button is clicked and canceled subsequnently", :js => true do
      # pending
      click_link "Delete Appointment"
      within(".bootbox.modal") do
        click_link "Cancel"
      end
      within("#appointments-container") do
        page.should have_selector("li")
      end
    end

    it "opens edit appointment form when clicked on edit, with correct data", :js => true do
      within("#appointments-container li") do
        click_link "Edit Appointment"
      end
      page.should have_selector("#add-appointment-form", :visible => true)
      
      within("#add-appointment-form") do
        find('#appointmentTitle').value.should eq('Awesome appointment')
        find('#appointmentDate').value.should eq('10-15-2012')
        find('#appointmentTime').value.should eq('11:30 am')
      end      
    end

    it "when edited saves an appointment with modified data", :js => true, :focus => true do
      within("#appointments-container li") do
        click_link "Edit Appointment"
      end
      within("#add-appointment-modal") do
        fill_in 'appointmentTitle', :with => 'My Awesome Appointment 2'
        find('#appointmentDate').value.should eq('10-15-2012')
        find('#appointmentTime').value.should eq('11:30 am')
        click_link('Save')
      end
      within("#appointments-container") do
        find('li').should have_content('My Awesome Appointment 2')
      end
    end
  end

end
