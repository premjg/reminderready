FactoryGirl.define do
  factory :appointment do |f|
    f.title "Awesome appointment"
    f.due_at Time.now
    f.association :user
  end
end
