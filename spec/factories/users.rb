FactoryGirl.define do
  factory :user do |f|
    f.sequence(:email) {|n| "test#{n}@jobready.com.au"}
    f.password "awesome"
    f.name "John Smith"
    f.payroll_number Random.new.rand(1000..9999).to_s
    f.phone_number "+61 2 " + (Random.new.rand(10000000..99999999)).to_s
  end
end
