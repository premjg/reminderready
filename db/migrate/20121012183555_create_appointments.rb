class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :user_id
      t.string :title
      t.datetime :due_at
      t.boolean :email
      t.boolean :sms

      t.timestamps
    end
  end
end
