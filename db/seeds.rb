# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.create({
  name: 'John Smith', 
  email:'test@jobready.com.au', 
  password:'awesome', 
  payroll_number: '12324356454', 
  phone_number:'+61 2 98765432'
})
user.appointments.create({due_at: 3.days.from_now, title: "Coffee with Karen", email:true, sms:false})
user.appointments.create({due_at: 72.minutes.from_now, title: "Lunch with Jack", email:false, sms:true})